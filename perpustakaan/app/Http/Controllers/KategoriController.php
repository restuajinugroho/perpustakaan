<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class KategoriController extends Controller
{
    public function index(){
        $title ='kategori';
        $data = \DB::table('kategori')->get();
        
        return view('layouts.part.kategori',compact('title','data'));
    }
    public function add(){
        $title ='Tambah Kategori';

        return view('layouts.part.kategori_add',compact('title'));

    }
    public function store(Request $request){
        $nama = $request->nama;
        
        \DB::table('kategori')-> insert([
            'name'=>$nama,
            'created_at'=>date('Y-m-d H:i;s'),
            'updated_at'=>date('Y-m-d H:i;s')
        ]);
        //\Session::flash('sukses','Data Kategori Berhasil di Simpan');
        return redirect('/kategori')->with('Sucess','Kategori berhasil disimpan');
    }
    public function edit($id){
        $dt= \DB::table('kategori')->where('id',$id)->first();
        $title = 'Edit Kategori';
        return view('layouts.part.kategori_edit', compact('title','dt'));
    }
   public function destroy($id){
       $dt= \DB::table('kategori')->where('id',$id)->delete();
        return redirect('/kategori')->with('success','Pertanyaan berhasil dihapus');
    }
}
