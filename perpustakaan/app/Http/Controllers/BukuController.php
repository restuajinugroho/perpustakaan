<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BukuController extends Controller
{
    public function index(){
        $title= 'List Buku';
        $data=\DB::table('kategori')->get();

        return view('layouts.book.buku',compact('title','data'));
    }
    public function add(){
        $title= 'List Buku';
        $kategori=\DB::table('kategori')->get();

        return view('layouts.book.buku_add',compact('title',));
    }
}
