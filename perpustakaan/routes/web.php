<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layouts.login');
});
Route::get('/create', function () {
    return view('layouts.create');
});

Auth::routes();
//kategori
Route::get('/kategori', 'KategoriController@index');//menu Utama Kategori
Route::get('/kategori/add', 'KategoriController@add');//membuat kategori baru
Route::post('/kategori/post','KategoriController@store');//menampilkan kategori
Route::get('/kategori/{id}/edit', 'KategoriController@edit');//edit data
Route::put('/kategori/{id}', 'KategoriController@edit');//update
Route::delete('/kategori/{id}', 'KategoriController@destroy');//delete data
//Buku
Route::get('/buku','BukuController@index');//menu utama buku
Route::get('/buku/add','BukuController@add');
//Route::get('/home', 'HomeController@index')->name('home');
//Route::get('/index', function () {
//    return view('layouts.index');
//});