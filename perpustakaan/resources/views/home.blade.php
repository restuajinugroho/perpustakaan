@extends('layouts.index')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header text-primary">Dashboard</div>

                <div class="card-body text-dark">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    You are logged in! <br>
                    Selamat Datang {{ Auth::user()->name}} di Perpustakaan
                     
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
