@extends('layouts.index')

@section('content')
<div class="container-fluid">
  <div class="card shadow mb-4">
    <div class="card">
      <div class="card-header py-3">
        <a href="/kategori/add" class="btn btn-flat btn-sm btn-primary "><i class="fa fa-plus"></i> Tambah Kategori </a>
      </div>
        <!-- /.card-header -->
        <div class="card-body">
          <table class="table table-bordered">
            <thead>                  
              <tr>
                <th style="width: 10px">#</th>
                <th>Nama Kategori</th>
                <th>Dibuat</th>
                <th style="width: 40px">Action</th>
              </tr>
            </thead>
            <tbody>
                @foreach ($data as $b=>$dt)
                 <tr role="row" class="even">
                 <td class="sorting_1 text-center">{{$b+1}}</td>
                 <td>{{$dt->name}}</td>
                 <td>{{$dt->created_at}}</td>
                 <td>
                   <div class="d-flex">
                 <a href="/kategori/{{$dt->id}}/edit" class="btn btn-warning btn-sm my-1 ml-1">Edit</a>
                  <!--<a href="" class="btn btn-danger btn-sm my-1 d-flex text-center">Delete</a>-->
                  <form action="/kategori/{{$dt->id}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <input type="submit" value="Delete"class="btn btn-danger btn-sm my-1 ml-1">
                    </form>
                  </div>                
                </td>
                 </tr>               
                 @endforeach
              </tbody>
          </table>
        </div>
        <!-- /.card-body -->
        <div class="card-footer clearfix">
          <ul class="pagination pagination-sm m-0 float-right">
            <li class="page-item"><a class="page-link" href="#">«</a></li>
            <li class="page-item"><a class="page-link" href="#">1</a></li>
            <li class="page-item"><a class="page-link" href="#">2</a></li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item"><a class="page-link" href="#">»</a></li>
          </ul>
        </div>
    </div>
    </div>
@endsection
