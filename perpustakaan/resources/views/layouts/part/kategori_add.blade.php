@extends('layouts.index')


@section('content')
<div class="card shadow-lg  col-md-9 mb-4">
<div class="card-header  py-5">
 <div class="row">
   <div class="col-md-8 col-md-offset-2">
      <div class="box-header mb-2">
        <h5>{{$title}}</h5>
      </div>
      <div class="box-body ">
        <form role="form" method="post" action="/kategori/post">
            @csrf
            <div class="box-body text-primary">
              <div class="form-group">
                <label for="exampleInputEmail1">Nama Kategori</label>
                <input type="text" name="nama" class="form-control" id="exampleInputEmail1" placeholder="">
              </div>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>          

      </div>
   </div>
  </div>
</div>
</div> 
@endsection
