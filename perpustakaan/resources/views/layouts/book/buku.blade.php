@extends('layouts.index')

@section('content')
<p>
    <a href="/buku/add" class="btn btn-flat btn-sm btn-success"><i class="fa fa-plus"></i>Tambah Buku </a>
</p>
<div class="card shadow mb-4">
<div class="card">
    <div class="card-header">
        <h3 class="card-title text-primary">{{$title}}</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <table class="table table-bordered myTable">
        <thead>                  
          <tr>
            <th style="width: 10px">#</th>
            <th>Judul</th>
            <th>Stock</th>
            <th>Penulis</th>
            <th>Created at</th>
          </tr>
        </thead>
        <tbody>
        <tbody>
            @foreach ($data as $e=>$dt)
            <tr>
            </tr>
            @endforeach
        </tbody>    
      </table>
    </div>
    <!-- /.card-body -->
    <div class="card-footer clearfix">
      <ul class="pagination pagination-sm m-0 float-right">
        <li class="page-item"><a class="page-link" href="#">«</a></li>
        <li class="page-item"><a class="page-link" href="#">1</a></li>
        <li class="page-item"><a class="page-link" href="#">2</a></li>
        <li class="page-item"><a class="page-link" href="#">3</a></li>
        <li class="page-item"><a class="page-link" href="#">»</a></li>
      </ul>
    </div>
</div>
</div>
    @endsection